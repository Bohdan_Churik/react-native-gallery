import React, {useState, useEffect} from 'react';
import {connect, useDispatch} from 'react-redux';
import {asyncGetData} from '../redux/action';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import axios from 'axios';

function Gallery({navigation}) {
  const [imgs, setImgs] = useState([]);

  const [isLoading, setIsLoading] = useState(true);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(asyncGetData);
    axios
      .get(
        'https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0',
      )
      .then(data => {
        setImgs(data.data);
        setIsLoading(false);
      })
      .catch(err => {
        console.log('Error!', err);
      });
  });

  const _renderItem = ({item}) => {
    const source = {uri: item.urls.small};
    const src = {uri: item.urls.regular};
    return (
      <View style={styles.imageContainer}>
        <View style={styles.userContainer}>
          <Image
            style={styles.avatar}
            source={{uri: item.user.profile_image.large}}
          />
          <Text style={styles.authorName}>{item.user.username}</Text>
        </View>
        <View>
          <TouchableOpacity
            onPress={() => navigation.navigate('Photo', {source: src})}>
            <Image style={styles.image} source={source} />
          </TouchableOpacity>
          <Text style={styles.description}>{item.description}</Text>
        </View>
      </View>
    );
  };

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="#000" />
      </View>
    );
  } else {
    return (
      <View>
        <FlatList
          style={styles.flatList}
          data={imgs}
          renderItem={_renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loadingContainer: {
    backgroundColor: '#fff',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    alignSelf: 'center',
    borderRadius: 10,
    elevation: 5,
    shadowColor: '#000',
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      width: 3,
      height: 3,
    },
    backgroundColor: '#fff',
    width: '95%',
    marginVertical: 15,
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  image: {
    marginBottom: 10,
    width: '100%',
    height: 300,
  },
  userContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  avatar: {
    marginRight: 5,
    height: 35,
    width: 35,
  },
  authorName: {
    fontSize: 19,
  },
  description: {
    fontSize: 18,
  },
});

export default connect(
  null,
  {asyncGetData},
)(Gallery);

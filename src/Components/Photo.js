import * as React from 'react';
import {View, Image, StyleSheet} from 'react-native';

function Photo({route}) {
  const {source} = route.params;
  return (
    <View>
      <Image style={styles.image} source={source} />
    </View>
  );
}

export default Photo;

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
  },
});

import axios from 'axios';
import {GET_DATA, SUCCESS_DATA, ERROR_DATA} from './types';
const getData = () => ({
  type: GET_DATA,
});
const successData = data => ({
  type: SUCCESS_DATA,
  payload: data,
});
const errorData = error => ({
  type: ERROR_DATA,
  payload: error,
});

const asyncGetData = () => dispatch => {
  getData();
  axios
    .get(
      'https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0',
    )
    .then(res => dispatch(successData(res.data)))
    .catch(error => dispatch(errorData(error)))
    .finally(() => getData());
};

export {asyncGetData};
